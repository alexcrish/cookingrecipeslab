
public interface IStateManager {
	
    public void previousState(int n);
 
    public void nextState(int n);
 
    public String getStatus();

}
