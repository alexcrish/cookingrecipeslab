import java.util.ArrayList;

public abstract class Processor {
	
	public Food[] ingredients;
	public Processor nextProcessor;
	public abstract Food process(Processor cp);
	public abstract Processor setIngredientList(ArrayList<Ingredient> ingList);

}
