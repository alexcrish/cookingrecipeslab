
public class ReadyToRecieveIngredientsState extends State{
	
	void next(ProductCreatorContext context) 
	{	
		context.state = new ReadyToProcessState();
	}

	void prev(ProductCreatorContext context) 
	{
		System.out.println("The package is in it's root state.");
	}

	String printStatus() 
	{
		return "Ready to recieve ingredients.";
	}

}
