
public class ReadyToCookState extends State {
	
	void next(ProductCreatorContext context) 
	{	
		context.state = new ReadyToRecieveIngredientsState();
	}

	void prev(ProductCreatorContext context) 
	{
		context.state = new ReadyToProcessState();
	}

	String printStatus() 
	{
		return "Ingredients recieved. Ready to cook/ recieve more ingredients";
	}

}
