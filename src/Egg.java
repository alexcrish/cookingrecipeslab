

public class Egg extends Ingredient {

	public Egg(int id, String n, float c, float ps, float f, float cs, float w, float fs) throws CloneNotSupportedException {
		super(id, n, c, ps, f, cs, w, fs);
		foodType = "Egg";
	}
	
	private boolean cooked = false;
	
	public Egg setCooked()
	{
		this.cooked = true;
		return this;
	}
	
	public boolean isCooked()
	{
		return cooked;
	}

	@Override
	public boolean canEat() {
		return cooked;
	}

	@Override
	public boolean canDrink() {
		// TODO Auto-generated method stub
		return !cooked;
	}
}
