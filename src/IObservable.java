
public interface IObservable extends IFood
{
	public void setVisible();
	public int getId();
	public void notify(IObserver o);
}
