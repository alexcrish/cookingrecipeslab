

public class Ingredient extends Food implements IIngredient {

	public Ingredient(int id, String n, float c, float ps, float f, float cs, float w,
			float fs) throws CloneNotSupportedException {
		super(n, c, ps, f, cs, w, fs);
		foodType = "Ingredient";
		//if(!(this instanceof IntermediateProduct))
		{
			FoodList.ingredients[id] = this;
		}
	}
	
	public static Ingredient[] ingredientList = new Ingredient[256];

	@Override
	public boolean canEat() {
		return false;
	}

	@Override
	public boolean canDrink() {
		return false;
	}
	
	
}
