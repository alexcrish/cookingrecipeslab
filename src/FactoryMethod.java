
import java.util.ArrayList;

public class FactoryMethod {
	
	private Food[] ingredients;
	private ProductFactory pf;
	private IntermediateProductFactory ipf;
	
	public FactoryMethod(RecipesCore coreIn)
	{
		this.pf = new ProductFactory();
		this.ipf = new IntermediateProductFactory();
	}
	
	public Food getResult(ArrayList<Ingredient> ingList)
	{
		if(ingList.size() == 1)
		{
			return process(ingList);
		}
		else 
		{
			return cook(ingList);
		}
	}
	
	public Food process(ArrayList<Ingredient> ingList)
	{
		Food ingr = ingList.get(0);
		ipf.setIngredients(new Food[] {ingr});
		return ipf.createFood();
	}
	
	public Food cook(ArrayList<Ingredient> ingList)
	{
		for(int i = 0; i < Recipe.recipes.length; i++)
		{
			if(this.cookWithMultipleIngredients(ingList, Recipe.recipes[i].ingredients)!=null)
			{
				return this.cookWithMultipleIngredients(ingList, Recipe.recipes[i].ingredients);
			}
		}
		return null;	
	}
	
	private Product cookWithMultipleIngredients(ArrayList<Ingredient> ingList, Food[] recipe)
	{
		ingredients = new Food[ingList.size()];

		for(int i = 0; i < ingList.size(); i++)
		{
			ingredients[i] = ingList.get(i);
		}
		
		int recipeIndex = 0;
		int ingredientsMatched = 0;
		while(recipeIndex < recipe.length)
		{
			for(int i = 0; i < ingredients.length; i ++)
			{
				if(ingredients[i] == recipe[recipeIndex])
				{
					ingredientsMatched++;
				}
			}
			recipeIndex++;
		}
		
		pf.setIngredients(recipe);
		
		return ingredientsMatched == recipe.length ? (Product)pf.createFood() : null;
	}
}