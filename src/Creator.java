import java.util.ArrayList;


public interface Creator extends IStateManager {

	public Food getResult(ArrayList<Ingredient> ingList);
}
