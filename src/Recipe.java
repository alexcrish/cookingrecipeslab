

public class Recipe {
	
	public Food[] ingredients;
	public static Recipe[] recipes = new Recipe[256];
	public String name;
	
	public Recipe(int id, String name, Food[] ingrs)
	{
		this.name = name;
		ingredients = ingrs;
		recipes[id] = this;
	}
	
	public static Recipe breadR = new Recipe(0, "����", new Food[] {FoodList.flour, FoodList.freshWater, FoodList.sugar, FoodList.salt, FoodList.sunflowerOil});
	public static Recipe saltWaterR = new Recipe(1, "������� ����", new Food[] {FoodList.freshWater, FoodList.salt});
	public static Recipe badJokeR = new Recipe(2, "���� �����", new Food[] {FoodList.sugar, FoodList.salt});
	public static Recipe sweetWaterR = new Recipe(3, "���� � �������", new Food[] {FoodList.sugar, FoodList.freshWater});
	public static Recipe porridgeR = new Recipe(4, "��������� ����", new Food[] {FoodList.milk, FoodList.grain});
	public static Recipe bananaCakeR = new Recipe(5, "��������� �����", new Food[] {FoodList.flour, FoodList.chickenEgg, FoodList.sunflowerOil, FoodList.milk, FoodList.sugar, FoodList.banana});
}
