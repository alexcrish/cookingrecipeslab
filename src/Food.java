
public class Food extends AbstractFood implements IFood, Cloneable {
	private String name;
	public float cal;
	public float proteins;
	public float fat;
	public float carbs;
	public float water;
	public float fibers;
	public int ammount;
	protected String foodType;
	
	public Food(String n, float c, float ps, float f, float cs, float w, float fs) throws CloneNotSupportedException
	{
		name = n;
		cal = c;
		proteins = ps;
		fat = f;
		carbs = cs;
		water = w;
		fibers = f;
		ammount = 1;
	}
	
	public Food(Food food)
	{
		name = food.name;
		cal = food.cal;
		proteins = food.proteins;
		fat = food.proteins;
		carbs = food.carbs;
		water = food.water;
		fibers = food.fibers;
		ammount = 1;
	}
	
	@Override
	public Food clone() throws CloneNotSupportedException
	{
		return (Food) super.clone();
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public boolean canEat() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canDrink() {
		// TODO Auto-generated method stub
		return true;
	}
}
