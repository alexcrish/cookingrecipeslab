
public class ReadyToProcessState extends State {

	void next(ProductCreatorContext context) 
	{	
		context.state = new ReadyToCookState();
	}

	void prev(ProductCreatorContext context) 
	{
		context.state = new ReadyToRecieveIngredientsState();
	}

	String printStatus() 
	{
		return "Ingredient recieved. Ready to process/ recieve more ingredients";
	}

}
