

public class Liquid extends Ingredient {

	public Liquid(int id, String n, float c, float ps, float f, float cs, float w,
			float fs) throws CloneNotSupportedException {
		super(id, n, c, ps, f, cs, w, fs);
		foodType = "Liquids";
	}

	@Override
	public boolean canEat() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canDrink() {
		// TODO Auto-generated method stub
		return true;
	}
}
