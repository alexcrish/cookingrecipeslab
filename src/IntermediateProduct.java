import javax.swing.JButton;

public class IntermediateProduct extends Ingredient implements IObservable {

	public IntermediateProduct(int id, String str, Ingredient in) throws CloneNotSupportedException 
	{
		super(id, str, in.cal, in.proteins, in.fat, in.carbs, in.water, in.fibers);
		raw = in;
		this.id = id;
		FoodList.ims[id] = this;
	}
	
	public int id;
	public Ingredient raw;
	private boolean unlocked = false;
	private JButton slot;
	public IObserver observer;
	
	public JButton getSlot()
	{
		return slot;
	}
	
	public void setSlot(JButton slot)
	{
		this.slot = slot;
		this.slot.setVisible(unlocked);
	}
	
	public int getId()
	{
		return id;
	}
	
	public boolean visible()
	{
		return unlocked;
	}

	@Override
	public void setVisible() 
	{		
		unlocked = true;
		this.notify(RecipesCore.instance);
	}

	@Override
	public void notify(IObserver o) 
	{
		o.update(this);
	}

}
