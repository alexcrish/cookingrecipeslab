
public class Product extends Food {
	
	public Product() throws CloneNotSupportedException
	{
		super("������� �������", 0, 0, 0, 0, 0, 0);
		foodType = "product";
	}
	
	public Product(int id, String n) throws CloneNotSupportedException {
		super(n, 0, 0, 0, 0, 0, 0);
		foodType = "product";
	}
	
	public Product(String n, Food[] foodIn) throws CloneNotSupportedException {
		super(n, 0, 0, 0, 0, 0, 0);
		foodType = "product";
		ingredients = foodIn;
		this.countValues();
	}
	
	public Product(Food foodIn, Food[] ings) throws CloneNotSupportedException
	{
		super(foodIn);
		foodType = "product";
		ingredients = ings;
	}
	
	private Food[] ingredients;
	
	public Product setIngredients(Food[] foodIn)
	{
		ingredients = foodIn;
		return this;
	}
	
	public Food[] getRecipe()
	{
		return ingredients;
	}
	
	public Product countValues()
	{
		for(int i = 0; i < ingredients.length; i ++)
		{
			this.cal += ingredients[i].cal;
			this.carbs += ingredients[i].carbs;
			this.fat += ingredients[i].fat;
			this.fibers += ingredients[i].fibers;
			this.proteins += ingredients[i].proteins;
			this.water += ingredients[i].water;
		}
		
		this.cal /= (int)ingredients.length;
		this.carbs /= (int)ingredients.length;
		this.fat /= (int)ingredients.length;
		this.fibers /= (int)ingredients.length;
		this.proteins /= (int)ingredients.length;
		this.water /= (int)ingredients.length;
		
		return this;
	}

	@Override
	public boolean canEat() 
	{
		return water < 100 || cal > 0;
	}

	@Override
	public boolean canDrink() {
		return water >= 100;
	}
}