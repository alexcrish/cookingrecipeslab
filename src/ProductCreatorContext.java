
import java.util.ArrayList;

public class ProductCreatorContext implements Creator {
	
	private ProductFactory pf;
	private IntermediateProductFactory ipf;
	private IStrategy strategy;
	public State state;
	private Processor processor;
	
	public ProductCreatorContext(RecipesCore coreIn)
	{
		this.pf = new ProductFactory();
		this.ipf = new IntermediateProductFactory();
		this.state = new ReadyToRecieveIngredientsState();
		this.processor = new FoodProcessor();
	}
	
	public void setStrategy(IStrategy is)
	{
		this.strategy = is;
	}
	
	public Food getResult(ArrayList<Ingredient> ingList)
	{
		int statesToSkip = 0;
		if(state instanceof ReadyToProcessState)
		{
			processor.setIngredientList(ingList);
			return processor.process(new SingleIngredientProcessor());
			
		}
		else 
		{
			this.setStrategy(new CookingStrategy().setFactory(pf));
		}
		
		this.nextState(statesToSkip + 1);
		return strategy.cook(ingList);
	}
	
    public void previousState(int n) 
    {
    	for(int i = 0; i < n; i++)
    	{
    		state.prev(this);
    	}
    }
 
    public void nextState(int n) 
    {
    	for(int i = 0; i < n; i++)
    	{
    		state.next(this);
    	}
    }
 
    public String getStatus() {
        return state.printStatus();
    }
	
}