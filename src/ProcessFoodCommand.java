
public class ProcessFoodCommand implements ICommand {

	private Window reciever;
	
	public ProcessFoodCommand(Window rec)
	{
		this.reciever = rec;
	}
	
	@Override
	public void exec() 
	{
		this.reciever.processFood();
	}

	@Override
	public void setReciever(Window win) 
	{
		this.reciever = win;
	}

}
