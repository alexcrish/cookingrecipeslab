
public class ProductFactory extends FoodFactory {

	Director dir = new Director();
	ProductBuilder builder = new CommonProductBuilder();
	
	@Override
	public Product createFood()  {
		
		dir.setProductBuilder(builder);
		try {
			dir.constructProduct(this.ingredients);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return dir.getProduct();
	}

	@Override
	public void setIngredients(Food[] ingrIn) {
		this.ingredients = ingrIn;
	}
}
