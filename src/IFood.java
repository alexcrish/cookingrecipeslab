
public interface IFood {
	
	public boolean canEat();
	public boolean canDrink();

}
