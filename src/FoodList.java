
public class FoodList {
	
	public static Ingredient[] ingredients = new Ingredient[256];
	public static IntermediateProduct[] ims = new IntermediateProduct[256];
	
	public static Food banana;
	public static Food chickenEgg;
	public static Food milk;
	public static Food sugar;
	public static Food freshWater;
	public static Food sunflowerSeeds;
	public static Food grain;
	public static Food salt;
	public static Food sunflowerOil;
	public static Food flour;
	
	static
	{
		try {
			banana 			= new Fruit(0, "�����", 96, 1.5F, 0.5F, 21, 1.7F, 74);
			chickenEgg 		= new Egg(1, "������� ����", 157, 12.7F, 11.5F, 0.7F, 0, 74.1F);
			milk 			= new Liquid(2, "������", 65, 3.2F, 3.6F, 4.8F, 0, 87.3F);
			sugar 			= new Ingredient(3, "�����", 399, 0, 0, 99.8F, 0, 0.1F);
			freshWater 		= new Liquid(4, "����", 0, 0, 0, 0, 0, 100F);
			sunflowerSeeds 	= new Seeds(5, "������ ����������", 601, 20.7F, 52.9F, 10.5F, 5, 8);
			grain 			= new Seeds(6, "����� �������", 305, 11.8F, 2.2F, 59.5F, 10.8F, 14);
			salt 			= new Ingredient(7, "����", 0, 0, 0, 0, 0, 0.2F);
			sunflowerOil    = new IntermediateProduct(8, "����� ������������", (Ingredient)sunflowerSeeds);
			flour 			= new IntermediateProduct(9, "����", (Ingredient)grain);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}