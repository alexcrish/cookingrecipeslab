
public abstract class State 
{
    abstract void next(ProductCreatorContext pkg);
    abstract void prev(ProductCreatorContext pkg);
    abstract String printStatus();
}
