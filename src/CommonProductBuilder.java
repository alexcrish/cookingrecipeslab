
public class CommonProductBuilder extends ProductBuilder {

	@Override
	public void buildComponents(Food[] ingrs) 
	{
		product.setIngredients(ingrs);
		try {
			product = new Product(new Food("", 0, 0, 0, 0, 0, 0), ingrs);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void buildInfo() 
	{
		Food[] ingredients = product.getRecipe();
		
		for(int i = 0; Recipe.recipes[i] != null; i++)
		{
			if(ingredients == Recipe.recipes[i].ingredients)
			{
				product.setName(Recipe.recipes[i].name);
			}
		}
		
		for(int i = 0; i < ingredients.length; i ++)
		{
			product.cal += ingredients[i].cal;
			product.carbs += ingredients[i].carbs;
			product.fat += ingredients[i].fat;
			product.fibers += ingredients[i].fibers;
			product.proteins += ingredients[i].proteins;
			product.water += ingredients[i].water;
		}
		
		product.cal /= (int)ingredients.length;
		product.carbs /= (int)ingredients.length;
		product.fat /= (int)ingredients.length;
		product.fibers /= (int)ingredients.length;
		product.proteins /= (int)ingredients.length;
		product.water /= (int)ingredients.length;
		
	}

}
