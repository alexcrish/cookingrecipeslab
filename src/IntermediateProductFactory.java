
public class IntermediateProductFactory extends FoodFactory {

	@Override
	public IntermediateProduct createFood() {
		if(ingredients.length != 1)
		{
			return null;
		}
		else
		{
			for(int i = 0; i < FoodList.ingredients.length; i++)
			{
				if(FoodList.ingredients[i] instanceof IntermediateProduct)
				{
					IntermediateProduct im = (IntermediateProduct)FoodList.ingredients[i];
					if(im.raw == ingredients[0])
					{
						return im;
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public void setIngredients(Food[] ingrIn) 
	{
		this.ingredients = ingrIn;
	}

}
