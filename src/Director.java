
public class Director {
	
	private ProductBuilder pb;
	
	public void setProductBuilder(ProductBuilder builder)
	{
		pb = builder;
	}
	
	public Product getProduct()
	{
		return pb.getProduct();
	}
	
	public void constructProduct(Food[] ings) throws CloneNotSupportedException
	{
		pb.createNewProduct();
		pb.buildComponents(ings);
		pb.buildInfo();
	}

}
