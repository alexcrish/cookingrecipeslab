import java.util.ArrayList;

public interface IStrategy {
		
	public Food cook(ArrayList<Ingredient> ingList);
	public IStrategy setFactory(FoodFactory pf);
}
