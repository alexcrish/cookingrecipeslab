
public interface IClonable {
	
	public abstract Object clone() throws CloneNotSupportedException;

}
