
public abstract class ProductBuilder {
	
	protected Product product;
	
	public Product getProduct()
	{
		return product;
	}
	
	public void createNewProduct() throws CloneNotSupportedException
	{
		product = new Product();
	}
	
	public abstract void buildComponents(Food[] foods);
	public abstract void buildInfo();

}
