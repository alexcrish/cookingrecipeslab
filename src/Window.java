import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;

public class Window extends JFrame {

	private JPanel contentPane;
	private JLabel item1;
	private JLabel item2;
	private JButton[] contents = new JButton[48];
	private JButton[] chosen = new JButton[16];
	private JButton[] emptySlots = new JButton[48];
	private JButton btnNewButton_1;
	private JTextPane textPane;
	private RecipesCore core;
	public JFrame mainFrame = this;
	private Food currentResult;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window frame = new Window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void updateRefrigerator(RecipesCore core)
	{
		for(int i = 0; i< 3; i ++)
		{
			for(int j = 0; j < 16; j ++)
			{
				int index = i*16+j;
				ImageIcon foodIcon = new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + "blank" + ".png");
				JButton slot = new JButton("", foodIcon);
				if(core.currentIngredients.size() > index+1)
				{
					Ingredient ingr = core.currentIngredients.get(index+1);
					if(core.currentIngredients.get(index+1) instanceof IntermediateProduct)
					{
						IntermediateProduct im = (IntermediateProduct)core.currentIngredients.get(index+1);
						im.setSlot(slot);
						im.observer = core;
						//core.update(im);
					}
						slot.setToolTipText(ingr.getName());
						slot.setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + ingr.getName() + ".png"));
						slot.addActionListener(new ActionListener() 
						{
							public void actionPerformed(ActionEvent arg0) 
							{
								core.context.nextState((core.chosenIngredients.size() ^ 2)/2);
								textPane.setText(core.context.getStatus());
								if(core.chosenIngredients.size() < 16)
								{
									core.chosenIngredients.add(ingr);
									chosen[core.chosenIngredients.size()-1].setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + ingr.getName() + ".png"));
									chosen[core.chosenIngredients.size()-1].setToolTipText(ingr.getName());
								}
							}
						});
				}
				else slot.setVisible(false);
				
				//emptySlots[index] = emptySlot;
				//emptySlots[index].setBounds(10+32*j, 240+i*32, 32, 32);
				//contentPane.add(emptySlots[index]);
				
				contents[index] = slot;
				contents[index].setBounds(10+32*j, 240+i*32, 32, 32);
				contentPane.add(contents[index]);
				
				
			}
		}
	}
	
	public void updateContents(RecipesCore core)
	{
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j ++)
			{
				int index = i*4+j;
				ImageIcon foodIcon = new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + "blank" + ".png");
				JButton slot = new JButton("", foodIcon);
				if(core.chosenIngredients.size() > index)
				{
					Ingredient ingr = core.chosenIngredients.get(index);
					slot.setToolTipText(ingr.getName());
					slot.setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + ingr.getName() + ".png"));
				}
				
				chosen[index] = slot;
				chosen[index].setBounds(74+32*j, 50+i*32, 32, 32);
				chosen[index].setIcon(foodIcon);
				contentPane.add(chosen[index]);
			}
		}
	}
	
	private void initLabels()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		item1 = new JLabel("�����������");
		item1.setHorizontalAlignment(SwingConstants.CENTER);
		item1.setBounds(10, 220, 512, 14);
		getContentPane().add(item1);
		
		item2 = new JLabel("�������� ����");
		item2.setHorizontalAlignment(SwingConstants.CENTER);
		item2.setBounds(-117, 25, 512, 14);
		getContentPane().add(item2);
		
		textPane = new JTextPane();
		textPane.setBounds(10, 363, 512, 128);
		contentPane.add(textPane);
	}
	
	public void processFood()
	{
		IObservable im = (IObservable)this.currentResult;
		im.setVisible();
		//core.update(o);
		FoodList.ingredients[im.getId()] = (IntermediateProduct)im;
		contents[1] = null;
		
		updateRefrigerator(core);
		mainFrame.repaint();
		mainFrame.revalidate();
	}

	/**
	 * Create the frame.
	 */
	public Window() {
		
		core = new RecipesCore(this);
		
		initLabels();
		updateContents(core);
		updateRefrigerator(core);

		JButton res = new JButton("Result");
		res.setBounds(330, 50, 128, 128);
		
		
		res.setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + "BlankResult" + ".png"));
		contentPane.add(res);
		
		JButton btnNewButton = new JButton("�������");
		btnNewButton.addActionListener(new ActionListener() 
		{ //Observer
			public void actionPerformed(ActionEvent arg0) 
			{
				Food result = core.getResult();
				if(result == null) return;
				if(result instanceof IntermediateProduct )
				{
					currentResult = result;
					core.executeOperation(0);
				}
				//RESULT
				BufferedImage img = null;
				int w=0, h=0;
				try {
					img = ImageIO.read(new File("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + result.getName() + ".png"));
					w = img.getWidth(null);
					h = img.getHeight(null);
				} catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				Image fgImage = null;
				try {
					fgImage = ImageIO.read(new File("C:\\Users\\Alex\\workspace\\CourseAPPOO\\Salt.png"));
				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
				
				BufferedImage finalImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = finalImage.createGraphics();
				g.drawImage(img, 0, 0, null);
				g.drawImage(fgImage, 0, 0, null);
				g.dispose();
				
				res.setIcon(new ImageIcon(finalImage));
				core.chosenIngredients.clear();
				try
				{
					result = result.clone();
				} 
				catch (CloneNotSupportedException e) 
				{
					e.printStackTrace();
				}
				for(int i = 0; i < 16; i ++)
				{
					chosen[i].setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + "BlankResult" + ".png"));
					textPane.setText("���������� �������: " 	+ result.getName() + "\n" +
							"���������� ��������: " 	+ result.cal + 
							   "\t �����: " 			+ result.proteins +
							   "\t ����: " 				+ result.fat +
							   "\t ��������: " 			+ result.carbs +
							   "\t ����: " 				+ result.water +
							   "\t ������� �������: " 	+ result.fibers);
				}
			}
		});
		btnNewButton.setBounds(221, 102, 89, 23);
		btnNewButton.setVerticalAlignment(SwingConstants.TOP);
		contentPane.add(btnNewButton);
		
		btnNewButton_1 = new JButton("�������� ����");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				core.chosenIngredients.clear();
				for(int i = 0; i < 16; i ++)
				{
					chosen[i].setIcon(new ImageIcon("C:\\Users\\Alex\\workspace\\CourseAPPOO\\src\\icons\\" + "BlankResult" + ".png"));
					
				}
			}
		});
		btnNewButton_1.setBounds(74, 189, 128, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel label = new JLabel("���������");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(133, 25, 512, 14);
		contentPane.add(label);
	}
}
