import java.util.ArrayList;

public class ProcessingStrategy implements IStrategy {

	protected FoodFactory factory;
	
	
	@Override
	public Food cook(ArrayList<Ingredient> ingList) 
	{
		Food ingr = ingList.get(0);
		factory.setIngredients(new Food[] {ingr});
		return factory.createFood();
	}

	@Override
	public IStrategy setFactory(FoodFactory pf) 
	{
		factory = pf;
		return this;
	}

}
