import java.util.ArrayList;

public class CookingStrategy implements IStrategy 
{
	private Food[] ingredients;
	protected FoodFactory factory;

	@Override
	public Food cook(ArrayList<Ingredient> ingList) 
	{
		for(int i = 0; i < Recipe.recipes.length; i++)
		{
			if(this.cookWithMultipleIngredients(ingList, Recipe.recipes[i].ingredients)!=null)
			{
				return this.cookWithMultipleIngredients(ingList, Recipe.recipes[i].ingredients);
			}
		}
		return null;
	}
	
	private Product cookWithMultipleIngredients(ArrayList<Ingredient> ingList, Food[] recipe)
	{
		ingredients = new Food[ingList.size()];

		for(int i = 0; i < ingList.size(); i++)
		{
			ingredients[i] = ingList.get(i);
		}
		
		int recipeIndex = 0;
		int ingredientsMatched = 0;
		while(recipeIndex < recipe.length)
		{
			for(int i = 0; i < ingredients.length; i ++)
			{
				if(ingredients[i] == recipe[recipeIndex])
				{
					ingredientsMatched++;
				}
			}
			recipeIndex++;
		}
		
		factory.setIngredients(recipe);
		
		return ingredientsMatched == recipe.length ? (Product)factory.createFood() : null;
	}

	@Override
	public IStrategy setFactory(FoodFactory pf) 
	{		
		this.factory = pf;
		return this;
	}
	
}
