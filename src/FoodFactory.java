
public abstract class FoodFactory {
	
	public Food[] ingredients;

	public abstract Food createFood();
	public abstract void setIngredients(Food[] ingrIn);

}
