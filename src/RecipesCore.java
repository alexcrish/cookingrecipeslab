
import java.util.ArrayList;
import java.util.Scanner;

public final class RecipesCore implements IObserver {
	
	public ArrayList<Ingredient> currentIngredients;
	public ArrayList<Ingredient> chosenIngredients;
	public Creator context;
	public static RecipesCore instance;
	public State state;
	private ArrayList<ICommand> commands = new ArrayList();
	private Window client;

	
	public RecipesCore(Window window)
	{
		instance = this;
		this.currentIngredients = new ArrayList<Ingredient>();
		this.chosenIngredients = new  ArrayList<Ingredient>();
		this.context = new ProductCreatorContext(this);
		this.client = window;
		this.initCommands();
		this.initIngredients();
	}
	
	public void initCommands()
	{
		this.commands.add(new ProcessFoodCommand(client));
	}

	@Deprecated
	public void run()
	{
		Scanner sc = new Scanner(System.in);
		int opt = -1;
		
		while(true)
		{
			System.out.println("�������� �����������. ������� 0 ����� ���������.");
			
			for(int i = 1; i < currentIngredients.size(); i++)
			{
				System.out.println(i + ". " + currentIngredients.get(i).getName());
			}
			
		L2: while(true)
			{
				opt = sc.nextInt();
				if(opt == 0)
				{
					break L2;
				}
				else
				{
					if(currentIngredients.get(opt) != null)
					{
						chosenIngredients.add(currentIngredients.get(opt));
					}
				}
			}
			
			Food result = context.getResult(chosenIngredients);
			if(result != null)
			{
				System.out.println("���������� �������: " 	+ result.getName());
				System.out.println("���������� ��������: " 	+ result.cal + 
								   "\t �����: " 			+ result.proteins +
								   "\t ����: " 				+ result.fat +
								   "\t ��������: " 			+ result.carbs +
								   "\t ����: " 				+ result.water +
								   "\t ������� �������: " 	+ result.fibers);
				System.out.println("����� ����: " + result.canEat());
				System.out.println("����� ����: " + result.canDrink());
			}
			this.chosenIngredients = new  ArrayList<Ingredient>();
			sc.nextInt();
		}
	}
	
	public Food getResult()
	{
		return context.getResult(chosenIngredients);
	}
	
	public void executeOperation(int i)
	{
		commands.get(i).exec();
	}
	
	public void initIngredients() 
	{
		this.addIngredient((Ingredient)null);

		for(int i = 0; i < FoodList.ingredients.length; i++)
		{
			if(FoodList.ingredients == null)
			{
				break;
			}
			else
			{
				this.addIngredient((Ingredient)FoodList.ingredients[i]);
			}
		}
	}
	
	public void addIngredient(Ingredient food)
	{
		if(currentIngredients.contains(food))
		{
			return;
		}
		currentIngredients.add(food);
	}

	@Override
	public void update(Object o) 
	{
		if(o instanceof IntermediateProduct)
		{
			IntermediateProduct im = (IntermediateProduct) o;
			im.getSlot().setVisible(true);
		}
	}
}
