import java.util.ArrayList;

public class FoodProcessor extends Processor {

	public Food process(Processor cp) 
	{
		cp.ingredients = this.ingredients;
		return cp.process(null);
	}

	@Override
	public Processor setIngredientList(ArrayList<Ingredient> ingList) {
		Food ingr = ingList.get(0);
		this.ingredients = new Food[] {ingr};
		return null;
	}

}
